from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

class Test_Craftsman_Suite:
    driver = ''

    # Test Cases are linked to each automated test by ID
    # Test Run results are printed after execution, including Pass/Fail rate

    def setup_method(self): # General setup before all tests are run
        self.driver = webdriver.Chrome() # Creating the webdriver object
        self.driver.implicitly_wait(5) # This makes it so that the script will wait up to 5 seconds for things to load

    def test_craftsman_ID7(self):
        self.driver.get('http://computer-database.herokuapp.com/computers')
        search = self.driver.find_element_by_id('add')
        search.click() # Click the add button after locating it above

        expected_url = 'http://computer-database.herokuapp.com/computers/new'
        actual_url = self.driver.current_url

        # Compare the current url to the expected url for the Add New Computer section
        assert expected_url == actual_url, f'Error. Expected text is {expected_url}, but actual text is {actual_url}'

    def test_craftsman_ID8(self):
        self.driver.get('http://computer-database.herokuapp.com/computers/new')

        expected_text = 'Add a New Computer'
        actual_text = self.driver.find_element(By.XPATH, '//*[@id="main"]/h1').text

        assert expected_text == actual_text, f'Error. Expected text is {expected_text}, but actual text is {actual_text}'

    def test_craftsman_ID22(self):
        self.driver.get('http://computer-database.herokuapp.com/computers')
        search = self.driver.find_element_by_xpath('//*[@id="main"]/table/tbody/tr[1]/td[1]/a')
        search.click()

        expected_text = 'Edit computer'
        actual_text = self.driver.find_element_by_xpath('//*[@id="main"]/h1').text

        assert expected_text == actual_text, f'Error. Expected heading text is {expected_text}, but actual heading text is {actual_text}'

    def test_craftsman_ID19_ID20(self):
        self.driver.get('http://computer-database.herokuapp.com/computers/new')
        search = self.driver.find_element_by_id('name')
        search.send_keys('0000TESTCRAFSTMANID19ID20') # Enter Computer Name after locating the field above
        search = self.driver.find_element_by_id('introduced')
        search.send_keys('1965-1-2') # Enter Introduction Date
        search = self.driver.find_element_by_id('discontinued')
        search.send_keys('2002-2-3')  # Enter Discontinuation Date
        search = self.driver.find_element_by_xpath('//*[@id="company"]')
        search.send_keys('x', Keys.ENTER) # Select 'Xerox' from the list

        search = self.driver.find_element_by_xpath('//*[@id="main"]/form/div/input') # Locate the button to add computer
        search.click() # Click the button

        expected_text = '0000TESTCRAFSTMANID19ID20' # Validate the Computer Name on the landing page
        actual_text = self.driver.find_element_by_xpath('//*[@id="main"]/table/tbody/tr[1]/td[1]/a').text
        assert expected_text == actual_text, f'Error. Expected heading text is {expected_text}, but actual heading text is {actual_text}'

        expected_text = '02 Jan 1965' # Validate the Introduced Date on the landing page
        actual_text = self.driver.find_element_by_xpath('//*[@id="main"]/table/tbody/tr[1]/td[2]').text
        assert expected_text == actual_text, f'Error. Expected heading text is {expected_text}, but actual heading text is {actual_text}'

        expected_text = '03 Feb 2002' # Validate the Discontinued Date on the landing page
        actual_text = self.driver.find_element_by_xpath('//*[@id="main"]/table/tbody/tr[1]/td[3]').text
        assert expected_text == actual_text, f'Error. Expected heading text is {expected_text}, but actual heading text is {actual_text}'

        expected_text = 'Xerox' # Validate the Company / Manufacturer on the landing page
        actual_text = self.driver.find_element_by_xpath('//*[@id="main"]/table/tbody/tr[1]/td[4]').text
        assert expected_text == actual_text, f'Error. Expected heading text is {expected_text}, but actual heading text is {actual_text}'

    def teardown_method(self):
        self.driver.quit()
